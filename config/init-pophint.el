(require 'pophint)
(require 'pophint-config)

(setq pophint:popup-max-tips 1000)
(setq pophint:switch-direction-p nil)
(setq pophint:select-source-method 'use-popup-char)
(setq pophint:select-source-chars "tregdbvc")
(setq pophint:popup-chars "aklimnservu")
(setq pophint:switch-source-selectors '(("Quoted"   . "q") ("Url/Path" . "u") ("Line"     . "l") ("Sym"      . "s")))

(pophint:set-allwindow-command pophint:do-flexibly)

(pophint-config:set-automatically-when-marking t)
(pophint-config:set-yank-immediately-when-marking nil)
(pophint-config:set-automatically-when-isearch nil)
(pophint-config:set-do-when-other-window t)
(pophint-config:set-relayout-when-rangeyank-start nil)
;(pophint-config:set-w3m-use-new-tab t)
;(pophint-config:set-goto-immediately-when-e2wm-array t)
;(pophint-config:set-automatically-when-e2wm-array t)
(pophint-config:set-kill-region-kill-ring-save nil)
(pophint-config:set-mark-direction 'forward)
(pophint-config:set-tag-jump-command find-tag)
(pophint-config:set-tag-jump-command find-function)
(pophint-config:set-tag-jump-command find-variable)
(pophint-config:set-isearch-yank-region-command isearch-yank-line)
(pophint-config:set-isearch-yank-region-command migemo-isearch-yank-line)

(define-key evil-normal-state-map (kbd "f") 'pophint:do-eww-anchor)

(define-key evil-normal-state-map (kbd "b") 'eww-back-url)
;(global-set-key (kbd "f")   'pophint:do-eww-anchor)

(add-hook 'Info-mode-hook
                    '(lambda () (local-set-key (kbd ";") 'pophint:do-info-ref))
                              t)

(add-hook 'help-mode-hook
                    '(lambda () (local-set-key (kbd ";") 'pophint:do-help-btn))
                              t)

(add-hook 'Custom-mode-hook
                    '(lambda () (local-set-key (kbd ";") 'pophint:do-widget))
                              t)

;(require 'init-keychord)
;k
;
;
;

