;(after 'evil

(defun my-dired-next-line (count)
"Move to next line, always staying on the dired filename."
(interactive "p")
(dired-next-line count)
(dired-move-to-filename))
(defun my-dired-previous-line (count)
"Move to previous line, always staying on the dired filename."
(interactive "p")
(dired-previous-line count)
(dired-move-to-filename))



(evil-define-key 'normal dired-mode-map "h" 'my-dired-up-directory)
(evil-define-key 'normal dired-mode-map "l" 'dired-find-alternate-file)
(evil-define-key 'normal dired-mode-map "g" 'ag-dired)
(evil-define-key 'normal dired-mode-map "o" 'dired-sort-toggle-or-edit)
(evil-define-key 'normal dired-mode-map "v" 'dired-toggle-marks)
(evil-define-key 'normal dired-mode-map "m" 'dired-mark)
(evil-define-key 'normal dired-mode-map "u" 'dired-unmark)
(evil-define-key 'normal dired-mode-map "U" 'dired-unmark-all-marks)
(evil-define-key 'normal dired-mode-map "c" 'dired-create-directory)
(evil-define-key 'normal dired-mode-map "q" 'kill-this-buffer)
(evil-define-key 'normal dired-mode-map "/" 'evil-search-forward)
(evil-define-key 'normal dired-mode-map "n" 'evil-search-next)
(evil-define-key 'normal dired-mode-map "N" 'evil-search-previous)
(evil-define-key 'normal dired-mode-map "a" 'my-dired-next-line)
(evil-define-key 'normal dired-mode-map "k" 'my-dired-previous-line)
;)
(provide 'init-dired)
