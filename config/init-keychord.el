(require 'key-chord)
(key-chord-mode 1)
(key-chord-define evil-normal-state-map ",," 'evil-force-normal-state)
(key-chord-define evil-visual-state-map ",," 'evil-change-to-previous-state)
(key-chord-define evil-insert-state-map ",," 'evil-normal-state)
(key-chord-define evil-replace-state-map ",," 'evil-normal-state)


(defun virun ()
    (interactive)
  (shell-command "make virun"))

(defun vitest ()
    (interactive)
  (shell-command "make vitest"))

(key-chord-define evil-normal-state-map ",r" 'virun)
(key-chord-define evil-visual-state-map ",r" 'virun)
(key-chord-define evil-insert-state-map ",r" 'virun)
(key-chord-define evil-replace-state-map ",r" 'virun)

(key-chord-define evil-normal-state-map ",t" 'vitest)
(key-chord-define evil-visual-state-map ",t" 'vitest)
(key-chord-define evil-insert-state-map ",t" 'vitest)
(key-chord-define evil-replace-state-map ",t" 'vitest)

(provide 'init-keychord)
