(require 'evil-leader)
;enable evil-leader in every buffer where evil is enabled.
(global-evil-leader-mode)

(evil-leader/set-leader ",")

(evil-leader/set-key
  "e" 'find-file
  "b" 'switch-to-buffer
  "k" 'kill-buffer)

(provide 'init-leaderevil)
